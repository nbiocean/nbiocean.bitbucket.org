#!/bin/bash
icsurl="https://calendar.google.com/calendar/ical/010ert3vvgi7gu6btqefkrur60%40group.calendar.google.com/public/basic.ics"
curl --max-time 60 -o "${HOME}/www/themod/basic.ics" $icsurl

# for cron:
#*/30 * * * * ${HOME}/www/themod/update_ics.sh
# echo "*/10 * * * * ${HOME}/www/themod/update_ics.sh &> /dev/null" | crontab -
