#!/usr/bin/python
"""Download the ICS calendar file for TheMod and remove bad line breaks

Scheduling command
------------------
echo "*/10 * * * * /home/gfy-3/bluthgen/www/themod/update_ics.py &> /dev/null" | crontab -
"""
import urllib2
import contextlib
import re

sourceurl = 'https://www.google.com/calendar/ical/010ert3vvgi7gu6btqefkrur60%40group.calendar.google.com/public/basic.ics'

targetfname = '/home/gfy-3/bluthgen/www/themod/basic.ics'

multin = re.compile('\n{2,}')

def writelout(fout,lout):
    if lout:
        lout = lout.strip()+'\n'
        lout = multin.sub('\n',lout) # replace multiple line breaks in a row by one
        fout.write(lout.strip()+'\n')

if __name__ == "__main__":

    with contextlib.closing(urllib2.urlopen(sourceurl)) as fin:
        with open(targetfname, 'w') as fout:
            lout = ''
            for lin in fin:
                if lin.startswith(' '):
                    # leading whitespace marks lines where Google Calendar inserted
                    # line breaks that should not be there
                    lout += lin.strip().replace('\r\n','')
                else:
                    writelout(fout,lout)
                    lout=lin.replace('\r\n','')
            else:
                writelout(fout,lout)



