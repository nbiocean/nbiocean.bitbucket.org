# Team Ocean at the Niels Bohr Institute - Project and Workshop websites

## Static page

See the page live at https://nbiocean.bitbucket.io


## Maintenance

The site is based on the [Foundation framework](http://foundation.zurb.com/) ([version 4](http://foundation.zurb.com/docs/v/4.3.2/)).

TODO: Upgrade to the newest version of Foundation. Basically replace the folders `javascripts` and `stylesheets` and make some changes to style names in the html files.