Building blocks of ocean model DSL
==================================
.. categories:: Implementation

Concept
-------

At the `Bornö 2015 CFD workshop <http://www.gfy.ku.dk/~bluthgen/bornoe2015-cfd/>`_, the eScience group with Brian Vinter presented a concept for a ocean/climate modelling domain-specific language.

.. image:: Borno2015CFD-Ocean-DSL-blackboard.jpg
    :width: 100%

Input
~~~~~

With an ocean model DSL, writing and developing an ocean model requires only these steps:

#. Define a system of equations for the ocean model (shallow water, ..., primitive equations, ...?) in some high-level conceptual language (to start with, we will use Python instead)
#. Specify a domain in terms of geometry and boundaries (global/regional ocean or idealized basins with/without topography)
#. Specify a discretization (unstructured grids, regular grids, ...)
#. Specify the target: What should be included in the output? Data postprocessing (online/offline)?

Automatized
~~~~~~~~~~~

The system will then automatically

#. Select the optimal numerical method to solve the equations (although user input can be respected, too)
#. Generate code for fast execution, using the `Bohrium <https://bohrium.readthedocs.org/>`_ backend, on any infrastructure.


Example Model
-------------
Example set of governing equations and boundary conditions for an ocean model (`primitive equations <https://en.wikipedia.org/wiki/Ocean_dynamics#Primitive_equations>`_)

Prognostic variables 
~~~~~~~~~~~~~~~~~~~~
.. math::
    u, v, w, \rho, T, S, p

Forcings / knowns
~~~~~~~~~~~~~~~~~
.. math::
    \rho_0, \alpha, \beta, \kappa, f, g, H, \mathrm{precip}

Time derivatives
~~~~~~~~~~~~~~~~
* Momentum equations:

.. math::
    u_t + u u_x - fv = \frac1\rho_0 p_x + \kappa u_{xx}

.. math::
    v_t + u v_x + fu = \frac1\rho_0 p_y + \kappa v_{yy}

* Temperature and salinity

 .. math::
    T_t + \vec{u} \nabla \vec{T} = Q

 .. math::
    S_t + \vec{u} \nabla \vec{S} = \mathrm{precip}

Diagnostic relations
~~~~~~~~~~~~~~~~~~~~
* Pressure

.. math::
    p = \int_z^\eta \, \rho \, g \, dz

* Density

.. math::
    \rho = \rho_0 (1 + \beta S - \alpha T)

* Diffusivity (from mesoscale eddies):

.. math::
    K = f(\rho, h, \dots)
  
This could be formulated in various ways, e.g. computed from high-res non-hydrostatic slices (super-parameterization), a coarser run, or another model run

Conservation laws
~~~~~~~~~~~~~~~~~
.. math::
    u_x + v_y + w_z = 0

Boundary conditions
~~~~~~~~~~~~~~~~~~~
* No normal flow at the boundaries: :math:`u,v,w = 0`
* Wind stress at the surface: :math:`\kappa v_z = \tau^y`, :math:`\kappa u_z = \tau^x`
* Heat/temperature restoring: :math:`H = (T_1 - T_0) \frac{d}{\lambda_1}`
* Salinity restoring: :math:`S = (S_1 - S_0) \frac{d}{\lambda_2}`

Initial conditions
~~~~~~~~~~~~~~~~~~
* Velocities: E.g. :math:`u,v,w = 0` (ocean at rest)
* Temperature and salinity: some climatological values (long-term means from obsevations)

Interfaces
~~~~~~~~~~
* Composite systems (e.g. coupled to an atmospheric model)
* As additional boundary condition?


Implementation in the DSL
-------------------------
#. Formulate time derivatives of the prognostic equations

To do
-----
#. Establish a common terminology based on geophysics


